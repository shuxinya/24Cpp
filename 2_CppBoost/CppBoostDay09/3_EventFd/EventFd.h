#ifndef __EVENTFD_H__
#define __EVENTFD_H__

#include <functional>

using std::function;

class EventFd
{
    using EventFdCallback = function<void()>;
public:
    EventFd(EventFdCallback &&cb);
    ~EventFd();

    //启动与停止
    void start();
    void stop();

    //唤醒(执行write)
    void wakeup();

private:
    //创建用于通信的文件描述符
    int createEventFd();
    //阻塞等待（也就是执行read）
    void handleRead();

private:
    int _evfd;//eventfd进行通信的文件描述符
    EventFdCallback _cb;//需要做的任务
    bool _isStarted;//表识是否运行的标志

};

#endif
