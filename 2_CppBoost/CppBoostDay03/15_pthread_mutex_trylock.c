#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret != 0) \
    { \
        printf("%s : %s \n", funcName, strerror(ret));\
    } \
}while(0);

int main(int argc, char *argv[])
{
    pthread_mutex_t mutex;//创建互斥锁变量
    int ret = pthread_mutex_init(&mutex, NULL);
    ERROR_CHECK(ret, "pthread_mutex_init");
    //....
    //...
    //尝试上锁(尝试上锁两次)
    ret = pthread_mutex_lock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_trylock1");

    //直接上锁与尝试上锁，也就是pthread_mutex_lock与
    //pthread_mutex_trylock是不一样的，两次执行
    //pthread_mutex_lock会让程序处于阻塞状态，函数调用
    //并不会有返回结果；但是使用尝试上锁函数
    //pthread_mutex_trylock的时候，如果上锁不成功会立马
    //返回，不会有任何的影响,就相当于与此次尝试上锁没有
    //执行一样的
    printf("begin\n");
    ret = pthread_mutex_trylock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_trylock2");
    printf("end\n");

    //解锁
    ret = pthread_mutex_unlock(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_unlock");
    printf("end222\n");
    
    //进行销毁锁的时候，锁肯定是没有上锁的
    ret = pthread_mutex_destroy(&mutex);
    ERROR_CHECK(ret, "pthread_mutex_destroy");
    printf("end333\n");
    return 0;
}

