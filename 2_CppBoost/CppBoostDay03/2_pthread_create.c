#include <stdio.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#define ERROR_CHECK(ret, funcName) \
do{ \
    if(ret != 0) \
    { \
        printf("%s : %s \n", funcName, strerror(ret));\
        exit(1); \
    } \
}while(0);

void *threadFunc(void *arg)
{
    printf("I'm child thread\n");

    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_t thid;
    int ret = pthread_create(&thid, NULL, threadFunc, NULL);
    ERROR_CHECK(ret, "pthread_create");

    //让主线程睡觉，将CPU的控制权交给子线程
    usleep(100);

    //线程的执行是随机的
    printf("I'm main thread\n");
    return 0;
}



