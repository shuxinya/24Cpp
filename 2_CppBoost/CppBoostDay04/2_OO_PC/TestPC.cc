#include "Producer.h"
#include "Consumer.h"
#include "TaskQueue.h"
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

void test()
{
    TaskQueue  taskQue(10);//创建了唯一的TaskQueue对象
    unique_ptr<Thread> pro(new Producer(taskQue));
    unique_ptr<Thread> con(new Consumer(taskQue));

    //生产者与消费者启动
    pro->start();
    con->start();

    //让主线程等待子线程的退出
    pro->stop();
    con->stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

