#include "ThreadPool.h"
#include <unistd.h>

ThreadPool::ThreadPool(size_t threadNum, size_t queSize)
: _threadNum(threadNum)
, _queSize(queSize)
, _taskQue(_queSize)
, _isExit(false)
{
    _threads.reserve(_threadNum);
}

ThreadPool::~ThreadPool()
{
    if(!_isExit)
    {
        stop();
        _isExit = true;
    }
}

void ThreadPool::start()
{
    for(size_t idx = 0; idx < _threadNum; ++idx)
    {
        //这里有所改变==============================================
        //每个线程在创建的时候，将线程的名字传递进来
       unique_ptr<Thread> up(new Thread(std::bind(&ThreadPool::threadFunc, this),
                                        std::to_string(idx)));
       _threads.push_back(std::move(up));
    }

    for(auto &th : _threads)
    {
        th->start();
    }
}

void ThreadPool::addTask(Task &&task)
{
    if(task)
    {
        _taskQue.push(std::move(task));
    }
}

Task ThreadPool::getTask()
{
    return _taskQue.pop();
}

void ThreadPool::stop()
{
    //只要任务队列中有数据，线程池中的工作线程就不能退出，让其sleep
    while(!_taskQue.empty())
    {
        sleep(1);
    }

    _isExit = true;

    //将所有等在在_notEmpty上的工作线程全部唤醒
    _taskQue.wakeup();

    for(auto &th : _threads)
    {
        th->stop();
    }
}

//在线程池中封装的任务，这个任务的实际执行者WorkThread
void ThreadPool::threadFunc()
{
    while(!_isExit)
    {
        Task taskcb = getTask();
        if(taskcb)
        {
            taskcb();
        }
    }
}
