#ifndef __TIMERFD_H__
#define __TIMERFD_H__

#include <functional>

using std::function;

class TimerFd
{
    using TimerFdCallback = function<void()>;
public:
    TimerFd(TimerFdCallback &&cb, int initSec, int peridocSec);
    ~TimerFd();

    //启动与停止
    void start();
    void stop();

private:
    //创建用于通信的文件描述符
    int createTimerFd();
    //阻塞等待（也就是执行read）
    void handleRead();
    //设置定时器
    void setTimerFd(int initSec, int peridocSec);

private:
    int _timerfd;//timerfd_create创建的文件描述符
    TimerFdCallback _cb;//需要做的任务
    bool _isStarted;//表识是否运行的标志
    int _initSec;//初始时间
    int _peridocSec;//周期时间

};

#endif
