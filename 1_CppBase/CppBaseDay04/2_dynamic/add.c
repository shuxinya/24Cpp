
//函数名字叫做add
//函数的返回类型是int
//函数的参数列表是int,int
//函数的形参是x,y
//大括号范围就是add函数的函数体
//大括号里面的代码是函数的实现
int add(int x, int y) 
{
    return x + y;
}
