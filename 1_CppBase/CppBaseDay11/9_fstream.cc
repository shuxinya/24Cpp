#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::cin;
using std::fstream;
using std::string;

void test()
{
    //对于文件输入输出流而言，当文件不存在的时候，就打开失败
    //当文件存在的时候，才能进行打开成功
    string fileName("wuhan.txt");
    fstream fs(fileName);
    if(!fs.good())
    {
        cerr << "fstream is not good" << endl;
        return;
    }

    //通过键盘作为输入，输入五个int类型的数据，
    //将数据写入到文件中；再将文件中的内容进行输出，
    //打印到屏幕上
    int number = 0;
    for(size_t idx = 0; idx < 5; ++idx)
    {
        cin >> number;
        fs << number << " ";
    }

    //将文件流对象放在文件开头的位置就可以了
    //查看文件的长度tellg/tellp   g = get p = put
    //文件流对象的偏移seekg/seekp
    size_t pos = fs.tellg();
    cout << "pos = " << pos << endl;
    //seekp/seekg文件流对象的偏移
    /* fs.seekp(0); */
    /* fs.seekp(0, std::ios::beg); */
    fs.seekp(-pos, std::ios::end);
    for(size_t idx = 0; idx < 5; ++idx)
    {
        /* cout << "fs.failbit = " << fs.fail() << endl; */
        /* cout << "fs.eofbit = " << fs.eof() << endl; */
        /* cout << "fs.goodbit = " << fs.good() << endl; */
        fs >> number;
        cout << number << " ";
    }

    fs.close();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

