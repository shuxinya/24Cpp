#include <iostream>
#include <sstream>
#include <string>

using std::cout;
using std::endl;
using std::stringstream;
using std::string;

void test()
{
    int number1 = 10;
    int number2 = 20;
    stringstream ss;
    //字符串输入输出流的输出功能
    ss << "number1= " << number1 
       << " ,number2= " << number2 << endl;
    cout << ss.str();

    string key;
    int value;
    //字符串输入输出流的输入功能
    while(ss >> key >> value)
    {
        cout << key << "   " <<value << endl;
    }
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

