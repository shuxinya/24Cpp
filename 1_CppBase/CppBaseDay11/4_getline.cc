#include <iostream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;

void test()
{
    //对于文件输入流而言，当文件不存在的时候，就打开失败
    //当文件存在的时候，才能进行正常打开文件
    ifstream ifs("wd.txt");
    if(!ifs.good())
    {
        cerr << "ifstream is not good" << endl;
        return;
    }
    //对文件进行读操作
    string line[100];//读指定的任意一行
    size_t idx = 0;
    //对于文件输入流而言，默认是以空格为分隔符
    while(getline(ifs, line[idx]))
    {
        cout << line[idx] << endl;
        ++idx;
    }
    
    cout << "line[9] = " << line[9] << endl;
    cout << "line[36] = " << line[36] << endl;
    ifs.close();
}
int main(int argc, char *argv[])
{
    test();
    return 0;
}

