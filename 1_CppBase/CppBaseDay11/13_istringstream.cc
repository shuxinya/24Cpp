#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::istringstream;
using std::ifstream;
using std::string;

void readConf(const string &filename)
{
    ifstream ifs(filename);
    if(!ifs)
    {
        cerr << "ifstream is not good" << endl;
        return;
    }

    string line;
    while(getline(ifs, line))
    /* while(ifs >> key >> value) */
    {
        //iss是输入流
        istringstream iss(line);
        string key, value;
        iss >> key >> value;
        cout << "key = " << key << ",value = " << value << endl;
    }

    ifs.close();
}
void test()
{
    readConf("conf.data");
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

