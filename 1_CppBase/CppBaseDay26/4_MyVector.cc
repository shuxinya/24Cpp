#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::allocator;

template<typename T>
class Vector
{
public:
    using iterator = T *;//

    iterator begin()
    {
        return _start;
    }

    iterator end()
    {
        return _finish;
    }
    Vector()
    : _start(nullptr)
    , _finish(nullptr)
    , _end_of_storage(nullptr)
    {
        cout << "Vector()" << endl;
    }

    ~Vector();

    void push_back(const T &value); 
    void pop_back();    

    int size() const;
    int capacity() const;
private:
    void reallocate();//重新分配内存,动态扩容要用的
private:    
    static allocator<T> _alloc;

    T * _start;          //指向数组中的第一个元素
    T * _finish;         //指向最后一个实际元素之后的那个元素
    T * _end_of_storage; //指向数组本身之后的位置
};

template <typename T>
allocator<T> Vector<T>::_alloc;

template <typename T>
Vector<T>::~Vector()
{
    cout << "~Vector()" << endl;
    if(_start)
    {
        while(_start != _finish)
        {
            //销毁对象
            _alloc.destroy(--_finish);
        }
        //将空间回收
        _alloc.deallocate(_start, capacity());
    }
}

template <typename T>
void Vector<T>::push_back(const T &value)
{
    /* if(_finish == _end_of_storage) */
    if(size() == capacity())
    {
        //需要扩容
        reallocate();
    }

    if(size() < capacity())
    {
        //构建对象
        _alloc.construct(_finish++, value);
    }
}

template <typename T>
void Vector<T>::pop_back()
{
    if(size() > 0)
    {
        //销毁对象
        _alloc.destroy(--_finish);
    }
}

template <typename T>
int Vector<T>::size() const
{
    return _finish - _start;
}

template <typename T>
int Vector<T>::capacity() const
{
    return _end_of_storage - _start;
}

template <typename T>
void Vector<T>::reallocate()//重新分配内存,动态扩容要用的
{
    //1、计算出老的空间的大小，以及新的空间的大小
    int oldCapacity = capacity();
    int newCapacity = 2 * oldCapacity > 0 ? 2 * oldCapacity : 1;

    //2、申请新空间
    T *ptmp  = _alloc.allocate(newCapacity);

    if(_start)
    {
        //3、将老的空间上到元素拷贝到新的空间来，然后将老的空间上的
        //元素销毁
        std::uninitialized_copy(_start, _finish, ptmp);
        while(_start != _finish)
        {
            //销毁对象
            _alloc.destroy(--_finish);
        }
        //4、将老的空间回收
        _alloc.deallocate(_start, oldCapacity);

    }
    //5、三个指针指向新的空间
    _start = ptmp;
    _finish = ptmp + oldCapacity;
    _end_of_storage = ptmp + newCapacity;
}

void printCapacity(const Vector<int> &con)
{
    cout << "size() = " << con.size() << endl;
    cout << "capacity() = " << con.capacity() << endl;
}

void test()
{
    Vector<int> vec;
    printCapacity(vec);

    cout << endl;
    vec.push_back(1);
    printCapacity(vec);

    cout << endl;
    vec.push_back(2);
    printCapacity(vec);

    cout << endl;
    vec.push_back(3);
    printCapacity(vec);

    cout << endl;
    vec.push_back(4);
    printCapacity(vec);

    cout << endl;
    vec.push_back(5);
    printCapacity(vec);

    cout << endl;
    vec.push_back(6);
    printCapacity(vec);

    cout << endl;
    vec.push_back(7);
    printCapacity(vec);

    cout << endl;
    vec.push_back(5);
    printCapacity(vec);

    cout << endl;
    vec.push_back(5);
    printCapacity(vec);

    cout << endl;
    for(auto &elem : vec)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

