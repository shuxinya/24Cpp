#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;
using std::function;

int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    return x + y;
}

class Example
{
public:
    int add(int x, int y)
    {
        cout << "int Example::add(int, int)" << endl;
        return x + y;
    }

    int data = 10000;
};

void test()
{
    //C中的函数，函数名就是函数的入口地址
    //函数的形态（类型）:函数的返回类型 + 参数列表
    //函数的参数列表：参数的个数、参数的顺序、参数的类型
    //int(int, int)--->int()
    //function可以存函数类型，可以将其称为函数的容器
    function<int()> f = bind(&add, 1, 3);
    cout << "f() = " << f() << endl;

    cout << endl;
    Example ex;
    //int(Example *, int, int)--->int()
    function<int()> f3 = bind(&Example::add, &ex, 30, 70);
    cout <<"f3() = " << f3() << endl;

    cout << endl;
    function<int()> f2 = bind(&Example::add, ex, 30, 70);
    cout <<"f2() = " << f2() << endl;

    cout << endl;
    //占位符
    //int(int, int)---->int(int)
    function<int(int)> f4 = bind(add, 1, std::placeholders::_1);
    cout << "f4(9) = " << f4(9) << endl;

    cout << endl;
    Example ex2;
    //bind还可以绑定到数据成员上来
    function<int()> f5 = bind(&Example::data, &ex2);
    cout << "f5() = " << f5() << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

