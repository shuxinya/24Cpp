#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void printCapacity(const vector<int> &vec)
{
    cout << "size() = " << vec.size() << endl;
    cout << "capacity() = " << vec.capacity() << endl;
}

void test()
{
    vector<int> vec;
    vec.push_back(1);
    /* printCapacity(vec); */

    bool flag = true;

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << "  ";
        if(flag)
        {
            //底层发生了扩容，导致迭代器失效了
            vec.push_back(2);
            flag = false;
            /* printCapacity(vec); */
            it = vec.begin();
        }
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

