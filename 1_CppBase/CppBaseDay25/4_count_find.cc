#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::copy;

void func(int &value)
{
    cout << value << "  ";
}

void test()
{
    vector<int> vec = {1, 3, 5, 9, 7, 8, 6, 5};
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    cout << endl << endl;
    size_t cnt = count(vec.begin(), vec.end(), 15);
    cout << "cnt = " << cnt << endl;

    cout << endl << endl;
    auto it = find(vec.begin(), vec.end(), 7);
    if(it != vec.end())
    {
        cout << "查找成功 " << *it << "  ";
    }
    else
    {
        cout << "查找失败" << endl;
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

