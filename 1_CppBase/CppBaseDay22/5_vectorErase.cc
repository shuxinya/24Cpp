#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test()
{
    //删除所有的元素4
    vector<int> vec = {1, 3, 4, 6, 4, 4, 4, 4, 9, 7};
    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        if(4 == *it)
        {
            vec.erase(it);
        }
    }

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;
}

void test2()
{
    //删除所有的元素4
    vector<int> vec = {1, 3, 4, 6, 4, 4, 4, 4, 9, 7};
    for(auto it = vec.begin(); it != vec.end(); )
    {
        if(4 == *it)
        {
            //删除的时候，后面的元素会向前移动，所以此刻为了
            //防止漏掉某些元素，可以不去移动迭代器
            vec.erase(it);
        }
        else
        {
            ++it;
        }
    }

    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;
}
int main(int argc, char *argv[])
{
    test2();
    return 0;
}

