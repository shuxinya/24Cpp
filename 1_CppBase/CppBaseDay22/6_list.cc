#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::endl;
using std::list;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

struct ComparePoint
{
    bool operator()( const int& lhs, const int& rhs ) const
    {
        /* cout << "ComparePoint" << endl; */
        return lhs < rhs;
    }
};

void test()
{
    list<int> number = {1, 2, 4, 6, 8, 9, 6, 3, 6, 7, 6};
    display(number);

    cout << endl << "list的unique函数" << endl;
    number.unique();
    display(number);

    cout << endl << "list的reverse函数" << endl;
    number.reverse();
    display(number);

    cout << endl << "list的sort函数" << endl;
    /* number.sort();//默认按照从小到大排序 */
    /* number.sort(std::less<int>()); */
    /* number.sort(std::greater<int>()); */
    number.sort(ComparePoint());
    display(number);

    cout << endl << "list的unique函数" << endl;
    number.unique();
    display(number);

    cout << endl << "list的merge函数" << endl;
    list<int> number2 = {11, 33, 55, 99, 5, 22, 77};
    number2.sort();//需要将两个链表都排序
    /* number.sort(std::greater<int>()); */
    /* number2.sort(std::greater<int>()); */
    number.merge(number2);
    display(number);
    display(number2);//将number2中的元素都移到了number中
}

void test2()
{
    list<int> number = {1, 2, 4, 6, 8, 9, 6, 3, 6, 7, 6};
    display(number);

    list<int> number2{11, 33, 55, 88};
    display(number2);
    auto it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.splice(it, number2);
    display(number);
    display(number2);

    cout << endl << endl;
    list<int> number3{111, 333, 555, 888, 999, 777, 222};
    display(number3);
    it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    auto it33 = number3.end();
    --it33;
    --it33;
    cout << "*it33 = " << *it33 << endl;
    cout << "*it = " << *it << endl;
    number.splice(it, number3, it33);
    display(number);
    display(number3);

    cout << endl;
    auto it44 = number3.begin();
    auto it55 = number3.end();
    --it55;
    --it55;
    --it55;
    cout << "*it44 = " << *it44 << endl;
    cout << "*it55 = " << *it55 << endl;
    number.splice(it, number3, it44, it55);
    display(number);
    display(number3);

    cout << endl << "splice在同一个链表中进行操作" << endl;
    auto it4 = number.begin();
    ++it4;
    cout << "*it4 = " << *it << endl;
    auto it5 = number.end();
    --it5;
    --it5;
    cout <<"*it5 = " << *it5 << endl;
    number.splice(it4, number, it5);
    display(number);
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

