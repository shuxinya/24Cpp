#include <iostream>

using std::cout;
using std::endl;

//查查：引用与指针的区别？
void test()
{
    int number = 10;
    //引用是一个变量的别名,引用的提出是为了减少指针的使用
    int &ref = number;

    cout << "number = " << number << endl;
    cout << "ref = " << ref << endl;
    printf("&number = %p\n", &number);
    printf("&ref = %p\n", &ref);

    cout << endl;
    //操作引用与操作实体本身的效果是一样的
    number = 20;
    cout << "number = " << number << endl;
    cout << "ref = " << ref << endl;

    cout << endl;
    ref = 30;
    cout << "number = " << number << endl;
    cout << "ref = " << ref << endl;

    cout << endl;
    int value = 50;
    ref = value;
    cout << "value = " << value << endl;
    cout << "ref = " << ref << endl;
    cout << "number = " << number << endl;
    printf("&value = %p\n", &value);
    printf("&ref = %p\n", &ref);
    printf("&number = %p\n", &number);


    //引用不能单独存在，在定义的时候必须要进行初始化，
    //引用在定义的时候必须要绑定到一个变量上，一经绑定之后
    //就不能改变引用的指向，引用的本质就是一个指针，
    //本质是一个指针常量(* const)
    /* int &ref2;//error */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

