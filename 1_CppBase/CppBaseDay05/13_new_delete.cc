#include <stdlib.h>
#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

//什么内存泄漏？什么叫做野指针？内存越界？踩内存？
//内存踩踏?

//C中malloc与free和C++中的new与delete之间的异同点
//相同点
//1、都是用来申请堆空间的语法
//2、都需要成对出现malloc/free,new/delete(不能交叉使用)
//
//不同点
//1、malloc与free是C语言中的库函数，但是new/delete是C++中
//表达式
//2、malloc申请是原始的未初始化的空间，但是new申请的空间
//是可以进行初始化

//堆空间   栈空间  全局静态区
void test()
{
    //原始的未初始化的空间
    int *pret = (int *)malloc(sizeof(int));
    //清零操作
    memset(pret, 0, sizeof(int));
    //赋值
    *pret = 10;
    //....可以进行正常使用
    //....
    //回收malloc申请的堆空间
    free(pret);
    /* pret = NULL;//NULL是C中的写法 */
    pret = nullptr;//NULL是C中的写法
    //在C++11中，空指针nullptr进行替代
    //...还有可能会继续使用pret
}

void test2()
{
    //new是C++中申请堆空间的方式
    //可以申请堆空间，并且进行赋初值
    int *pInt = new int(10);
    
    *pInt = 20;
    cout << "*pInt = " << *pInt << endl;
    //....可以进行正常使用
    //....

    //回收堆空间
    delete  pInt;
    pInt = nullptr;
}

void test3()
{
    //new是C++中申请堆空间的方式
    //可以申请堆空间，并且进行赋初值
    /* //int arr[10] */
    //此处的小括号具备初始化的含义
    int *pInt = new int[10]();//此处表明申请的是10个int大小
    
    pInt[0] = 10;
    pInt[1] = 20;
    /* cout << "*pInt = " << *pInt << endl; */
    //....可以进行正常使用
    //....

    //回收堆空间数组
    delete [] pInt;
}
int main(int argc, char *argv[])
{
    test();
    return 0;
}

