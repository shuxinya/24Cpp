#include <iostream>

using std::cout;
using std::endl;

void test0()
{
    int value = 10;
    int *p = &value;
    //指针所指变量的内容
    cout << "*p = " << *p << endl;
    *p = 20;
    //指针的指向
    p = NULL;

}

//函数指针            指针函数
//int (*func)()       int*  func()
//数组指针            指针数组
//int (*array)[10]    int*  array[10]
void test()
{
    int a = 10;
    int number = 1;
    //const位于*的左边，将其称为常量指针
    //(常量指针不能改为指针的内容，但是可以改变指针的指向)
    const int *p1 = &number;
    cout << "*p1 = " << *p1 << endl;
    /* *p1 = 20;//error,常量不能赋值 */
    p1 = &a;//ok,可以改变指针的指向
    cout << "*p1 = " << *p1 << endl;

    cout << endl;
    int number2 = 1;
    //const位于*的左边，将其称为常量指针
    //(常量指针不能改为指针的内容，但是可以改变指针的指向)
    int const *p2 = &number2;
    cout << "*p2 = " << *p2 << endl;
    /* *p2 = 20;//error,常量不能赋值 */
    p2 = &a;//ok,可以改变指针的指向
    cout << "*p2 = " << *p2 << endl;

    cout << endl;
    int number3 = 100;
    //const位于*的右边，将其称为指针常量
    //(指针常量可以改边指针所指变量的内容，
    //但是不可以改变指针的指向)
    int * const p3 = &number3;
    cout << "*p3 = " << *p3 << endl;
    *p3 = 20;//ok,可以改变指针所指变量的内容
    /* p3 = &a;//error,不可以改变指针的指向 */
    cout << "*p3 = " << *p3 << endl;

    cout << endl;
    int number4 = 100;
    //双const，既不能改变指针所指变量的值，也不能改变指针的
    //指向(只读的特性)
    const int * const p4 = &number4;
    cout << "*p4 = " << *p4 << endl;
    /* *p4 = 20;//error，不可以改变指针所指变量的内容 */
    /* p4 = &a;//error,不可以改变指针的指向 */
    cout << "*p4 = " << *p4 << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

