#include <iostream>

using std::cout;
using std::endl;

//提出引用就是为了减少指针的使用

//2、引用作为函数的返回类型
int arr[10] = {1, 3, 5, 8};//arr是全局的数组

//函数的返回类型要用引用，有个前提：
//实体的生命周期一定要大于函数的生命周期
int &func(size_t idx)//size_t是无符号的整型
{
    return arr[idx];
}

int func2()
{
    int number = 10;
    return number;
}

//注意：不能返回局部变量的引用
int &func3()
{
    int number = 10;//number是局部变量，生命周期在大括号中
    return number;
}


void test()
{
    //还是利用引用是一个变量的别名，操作引用与操作变量本身
    //是一样的
    cout << "func(0) = " << func(0) << endl;
    arr[0] = 100;
    cout << "func(0) = " << func(0) << endl;
    func(0) = 2000;
    cout << "arr[0] = " << arr[0] << endl;
    cout << "func(0) = " << func(0) << endl;

    /* func2() = 300;//error */
}

//返回了堆空间的引用，也是返回了实体的生命周期比函数生命
//周期要大,不建议返回堆空间的引用，除非有自动内存回收的
//机制
int &func4()
{
    //申请了堆空间
    int *pInt = new int(100);

    return *pInt;
}

void test2()
{
    int a = 3, b = 5;
    int c = a + func4() + b;//存在内存泄漏
    cout << "c = " << c << endl;

    int &ref = func4();

    delete &ref;//就是在回收new的堆空间
}
int main(int argc, char *argv[])
{
    test2();
    return 0;
}

