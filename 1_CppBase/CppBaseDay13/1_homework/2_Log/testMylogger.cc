#include "Mylogger.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::to_string;

#if 0
string intToString(int value)
{
    ostringstream oss;
    oss << value;
    return oss.str();
}
#endif

//带参数的宏定义
#define prefix(msg) (string(__FILE__) + string(": ") \
                 + string(__FUNCTION__) + string(": ") \
                 + string(to_string(__LINE__)) + string(": ") \
                 + msg).c_str()
    
#define logError(msg) Mylogger::getInstance()->error(prefix(msg))
#define logInfo(msg) Mylogger::getInstance()->info(prefix(msg))
#define logWarn(msg) Mylogger::getInstance()->warn(prefix(msg))
#define logDebug(msg) Mylogger::getInstance()->debug(prefix(msg))

inline string func(const string &msg)
{
    string tmp = string(__FILE__) + string(": ")
                 + string(__FUNCTION__) + string(": ")
                 + string(to_string(__LINE__)) + string(": ")
                 + msg;

    return tmp;
}

void test0()
{
    Mylogger *plog = Mylogger::getInstance();
    plog->error("This is an error message");
}

void test1()
{
    cout << __FILE__ << "   "
         << __FUNCTION__ << "   "
         << __LINE__ << endl;
}

void test2()
{
    Mylogger::getInstance()
        ->error(prefix("This is an error message"));
    Mylogger::getInstance()
        ->info(prefix("This is an info message"));

}

void test4()
{
    logError("This is wangdao message");
    logWarn("This is warn message");
    logInfo("This is info message");
    logDebug("This is debug message");
}

void test()
{
    Mylogger::getInstance()->error("This is an error message");
    Mylogger::getInstance()->info("This is an info message");
    Mylogger::getInstance()->warn("This is an warn message");
    Mylogger::getInstance()->debug("This is an debug  message");

}

int main(int argc, char *argv[])
{
    test4();
    return 0;
}

