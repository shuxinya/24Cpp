#include <iostream>

using std::cout;
using std::endl;

//单例模式
//设计模式主要目的是想在整个系统中只能出现类的一个实例，即
//一个类只有一个对象

//创建这个对象之后，对象肯定要放在内存里面
//栈  堆  全局 静态  文字常量  程序代码
//栈  堆  全局 静态  
//
//怎么办呢？四个区域进行创建的时候有什么共同特点呢？
//都是在类的外面进行创建的对象
//
//能不能在类里面创建对象呢

class Singleton
{
public:
    static Singleton *getInstance()
    {
        if(_pInstance == nullptr)
        {
            _pInstance = new Singleton();
        }
        return _pInstance;
    }

    static void destroy()
    {
        /* if(_pInstance != nullptr) */
        if(_pInstance )
        {
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

private:
    Singleton()
    {
        cout << "Singleton()" << endl;
    }

    ~Singleton()
    {
        cout << "~Singleton()" << endl;
    }

private:
    static Singleton *_pInstance;
};

Singleton *Singleton::_pInstance = nullptr;

/* Singleton gS1;//全局,error */
/* Singleton gS2;//全局,error */

/* static Singleton ss1;//静态,error */
/* static Singleton ss2;//静态,error */

void test()
{
    Singleton *ps1 = Singleton::getInstance();
    Singleton *ps2 = Singleton::getInstance();
    cout << "ps1 = " << ps1 << endl;
    cout << "ps2 = " << ps2 << endl;

    ps1->destroy();
    ps2->destroy();
    Singleton::destroy();
    Singleton::destroy();
    /* delete ps1;//error */
    /* delete ps2;//error */
    /* Singleton s1;//栈上,error */
    /* Singleton s2;//栈上 */

    /* Singleton *pc = new Singleton();//堆，error */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

