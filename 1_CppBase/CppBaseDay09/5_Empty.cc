#include <iostream>

using std::cout;
using std::endl;

//空类 
class Empty
{
    //四个函数
#if 0
    Empty()
    {}
    Empty(const Empty &rhs)
    {}
    Empty &operator=(const Empty &rhs)
    {}
    ~Empty()
    {}
#endif

};

void test()
{
    //1 2 4 8 16  
    //为了去区分出不同的对象的
    cout << "sizeof(Empty) = " << sizeof(Empty) << endl;
    //空类可以创建对象吗,可以
    Empty em1;
    Empty em2;
    Empty em3;
    //这两个对象是用一个对象吗？
    printf("&em1 = %p\n", &em1);
    printf("&em2 = %p\n", &em2);
    printf("&em3 = %p\n", &em3);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

