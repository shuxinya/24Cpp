#include <iostream>
#include <map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::pair;
using std::make_pair;

void test()
{
    //Point pt[5] = { Point(1, 2), Point(3, 4) };
    //Point pt[5] = { {1, 2}, {3, 4} };
    //map的特征
    //1、存放的是pair类型，也即是key-value类型；
    //key值是唯一的，不能重复，value值可以重复，也可以不重复
    //2、默认情况下，会按照key值进行升序排列
    map<int, string> number = {
        pair<int, string>(1, "hello"),
        pair<int, string>(2, "world"),
        pair<int, string>(2, "world"),
        {6, "wuhan"},
        {7, "beijing"},
        make_pair(4, "hello"),
        make_pair(10, "world"),
        make_pair(6, "world")
    };

    /* map<int, string>::iterator it; */
    for(auto it = number.begin(); it != number.end(); ++it)
    {
        cout << it->first << "   " << it->second << endl;
    }

    cout << endl;
    size_t cnt = number.count(6);
    cout <<"cnt = " << cnt << endl;

     map<int, string>::iterator it = number.find(7);
     if(it == number.end())
     {
         cout << "该元素不在map中" << endl;
     }
     else
     {
         cout << "该元素存在map中 " 
              << it->first << "   "
              << it->second <<endl;
     }

     cout << endl;
     pair<map<int, string>::iterator, bool> ret = 
     /* auto ret = */ 
         /* number.insert(make_pair(3, "wangdao")); */
         /* number.insert({3, "wangdao"}); */
         number.insert(pair<int, string>(3, "wangdao"));
     if(ret.second)
     {
         cout << "插入成功 " << ret.first->first
              << "   " << ret.first->second << endl;
     }
     else
     {
         cout << "插入失败，该元素存在map中" << endl;
     }
     for(auto it = number.begin(); it != number.end(); ++it)
     {
         cout << it->first << "   " << it->second << endl;
     }

     cout << endl << endl;
     //map的下标中传递的是key类型
     //如果key值存在，就会返回value
     //如果key值不存在，就会返回key与空，并且将key与空插入到
     //了map中
     cout << "number[1] = " << number[1] << endl;//查找
     cout << "number[5] = " << number[5] << endl;//插入
     for(auto it = number.begin(); it != number.end(); ++it)
     {
         cout << it->first << "   " << it->second << endl;
     }
     cout << endl;
     number[5] = "beijing";//修改
     number[1] = "beijing";
     for(auto it = number.begin(); it != number.end(); ++it)
     {
         cout << it->first << "   " << it->second << endl;
     }
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

