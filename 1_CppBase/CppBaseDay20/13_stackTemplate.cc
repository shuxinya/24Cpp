#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

template <typename T = int, size_t sz = 10>
class Stack
{
public:
    Stack();
    ~Stack();
    bool empty() const;
    bool full() const;
    void push(const T &value);
    void pop();
    T top();
private:
    int _top;//栈顶指针
    T *_pdata;//存放元素的
};

template <typename T, size_t sz>
Stack<T, sz>::Stack()
: _top(-1)
, _pdata(new T[sz]())
{
    cout << "Stack()" << endl;
}

template <typename T, size_t sz>
Stack<T, sz>::~Stack()
{
    cout <<"~Stack()" << endl;
    if(_pdata)
    {
        delete [] _pdata;
        _pdata = nullptr;
    }
}

template <typename T, size_t sz>
bool Stack<T, sz>::empty() const
{
    return (-1 == _top);
}

template <typename T, size_t sz>
bool Stack<T, sz>::full() const
{
    return (_top == sz - 1);
}

template <typename T, size_t sz>
void Stack<T, sz>::push(const T &value)
{
    if(!full())
    {
        _pdata[++_top] = value;
    }
    else
    {
        cout << "The stack is full, not push" << endl;
        return;
    }
}

template <typename T, size_t sz>
void Stack<T, sz>::pop()
{
    if(!empty())
    {
        --_top;
    }
    else
    {
        cout << "The stack is empty, not pop" << endl;
        return;
    }
}

template <typename T, size_t sz>
T Stack<T, sz>::top()
{
    return _pdata[_top];
}

void test()
{
    Stack<int, 20> st;
    cout << "该栈是不是空的 " << st.empty() << endl; 
    st.push(1);
    cout << "该栈是不是满的 " << st.full() << endl; 

    for(size_t idx = 2; idx != 15; ++idx)
    {
        st.push(idx);
    }
    cout << "该栈是不是满的 " << st.full() << endl; 

    while(!st.empty())
    {
        cout << st.top() << "  ";
        st.pop();
    }
    cout << endl;
    cout << "该栈是不是空的 " << st.empty() << endl; 
}

void test2()
{
    Stack<string, 13> st;
    cout << "该栈是不是空的 " << st.empty() << endl; 
    st.push(string("aa"));
    cout << "该栈是不是满的 " << st.full() << endl; 

    for(size_t idx = 1; idx != 15; ++idx)
    {
        st.push(string(2, 'a' + idx));
    }
    cout << "该栈是不是满的 " << st.full() << endl; 

    while(!st.empty())
    {
        cout << st.top() << "  ";
        st.pop();
    }
    cout << endl;
    cout << "该栈是不是空的 " << st.empty() << endl; 
}
int main(int argc, char *argv[])
{
    test2();
    return 0;
}

