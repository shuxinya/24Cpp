#ifndef __ADD_H__
#define __ADD_H__

//对于模板而言，不能写成头文件与实现文件的形式，也就是不能将声明
//与实现分开，否则就会报错,也就是在链接的时候出现了问题
template <typename T>
T add(T x, T y);//声明

#include "add.cc"

#endif
