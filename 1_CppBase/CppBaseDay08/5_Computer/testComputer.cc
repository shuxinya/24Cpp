#include "Computer.h"
#include <iostream>

using std::cout;
using std::endl;

//思考题：本代码中能不能显示调用析构函数?
//不建议使用手动调用析构函数
void test0()
{
    //通过类名Computer创建了对象com
    Comupter com("Thinkpad", 5500);//com是栈对象
    com.print();

    cout << endl;
    /* com.~Comupter();//显示调用析构函数 */
    /* com.print(); */
}

//全局对象（全局变量）在进到main之前就已经初始化
Comupter gCom("mac", 20000);//gCom是全局对象

void test()
{
    //通过类名Computer创建了对象com
    Comupter com("Thinkpad", 5500);//com是栈对象
    com.print();

    cout << endl;
    int *pInt = new int(10);

    //对于堆对象而言，需要手动进行回收
    Comupter *pc = new Comupter("xiaomi", 6000);

    delete pc;
    pc = nullptr;

    delete pInt;
    pInt = nullptr;
}

int main(int argc, char *argv[])
{
    cout << "...start main..." << endl;
    test();
    cout << "-----finish test...." << endl;
    return 0;
}

