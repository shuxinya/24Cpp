#include "Computer.h"
#include <iostream>

using std::cout;
using std::endl;

void test()
{
    Comupter com("Thinkpad", 5500);//com是栈对象
    cout << "com = ";
    com.print();

    cout << endl;
    Comupter com2 = com;//拷贝构造函数
    cout << "com2 = ";
    com2.print();

    cout << endl;
    Comupter com3("mac", 20000);//拷贝构造函数
    cout << "com3 = ";
    com3.print();

    cout << endl << "com3 = com" << endl;
    com3 = com;//赋值运算符函数
    cout << "com3 = ";
    com3.print();

    cout << endl << "com3 = com3" << endl;
    com3 = com3;//赋值运算符函数
    cout << "com3 = ";
    com3.print();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

