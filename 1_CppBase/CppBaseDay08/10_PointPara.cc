#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    //默认情况下，编译器会生成拷贝构造函数
    //1、拷贝构造函数参数中的引用符号可以去掉吗？
    //A:不能去掉，因为如果去掉的话，那么在调用拷贝构造函数的
    //时候，就会出现用形参与实参结合，又满足拷贝构造函数的
    //调用时机，会一直循环调用下去，而函数的参数会入栈出栈，
    //而栈是有大小的，一直在入栈就会让栈空间爆满，也就是栈
    //溢出了(栈溢出)
    //
    //2、拷贝构造函数参数中的const可以去掉吗?
    //不能，如果传递的是右值的时候，非const左值引用不能
    //绑定到右值上
    Point(const Point &rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "Point(const Point &)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << ", " << _iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    Point pt1(1, 2);
    cout << "pt1 = ";
    pt1.print();

    cout << endl;
    /* Point(const Point rhs) */
    //const Point rhs = pt1
    //const Point x =  rhs
    //const Point y =  x
    //没有加引用就在无限循环
    /* Point(const Point &rhs) */
    //const Point &rhs = pt1
    Point pt2 = pt1;//拷贝构造函数
    cout << "pt2 = ";
    pt2.print();
}

Point func()
{
    Point pt2(4, 5);
    cout << "pt2 = ";
    pt2.print();

    return pt2;
}

void test2()
{
    int number = 10;
    int &ref = number;
    &number;
    const int &ref2 = 10;
    /* &10;//error,10是字面值常量，不能取地址,也是右值 */
    //不能将非const左值引用绑定到右值上
    /* Point(Point &rhs) */
    /* &func();//error,临时对象是右值，不能进行取地址 */
    const Point &rhs =  func();
    Point pt = func();//拷贝构造函数
    cout << "pt = ";
    pt.print();
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

