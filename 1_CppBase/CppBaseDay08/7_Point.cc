#include <iostream>

using std::cout;
using std::endl;

class Point
{
public:
    //构造函数的作用：就是为了初始化对象的数据成员的
    //构造函数不用也不能写返回类型的
    //
    //无参构造函数（默认构造函数）编译器会自动提供
    //如果自己实现了构造函数，那么编译器就不会再给我们
    //提供默认的无参构造函数；如果还想使用编译器提供的默认
    //构造函数，就需要自己显示的写出来
    //
    //构造函数是可以进行重载的
    
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    ,_iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    void print()
    {
        cout << "(" << _ix
             << ", " << _iy
             << ")" << endl;
    }

    //对象在销毁的时候会自动调用析构函数
    //析构函数的作用就是为了清理数据成员
    //编译器也会为我们提供析构函数
    
    //析构函数没有返回类型，也没有参数，析构函数是唯一的
    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    int a = 10;//a在栈区
    //创建对象的时候，会调用构造函数
    //进行数据成员的初始化
    //当用类创建对象的时候，构造函数会被自动调用
    Point pt(1, 2);//pt也在栈区，将pt称为栈对象
    pt.print();

    pt.~Point();//析构函数可以显示调用
    pt.print();
    /* pt.Point();//不能以对象加点的形式显示调用构造函数 */

    cout << "==============" << endl;
    //匿名对象，临时对象(用完之后立马就销毁了,其生命周期
    //只在本行)
    Point().print();//可以直接通过调用构造函数创建对象
    cout << "==============" << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

