#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

void test()
{
    //字符数组
    //C的字符串是以'\0'结尾的
    char str1[] = "hello";
    char str2[] = {'w', 'o', 'r', 'l', 'd', '\0'};
    /* char str2[] = "world"; */

    cout << "sizeof(str1) = " << sizeof(str1) << endl;
    cout << "sizeof(str2) = " << sizeof(str2) << endl;

    //int * const p3 
    str1[0] = 'H';//修改数组中的内容
    cout << "str1 = " << str1 << endl;
    /* str1 = nullptr;//error */

    //字符指针
    const char *pstr = "hello,world";
    cout << "pstr = " << pstr << endl;
    cout << "strlen(pstr) = " << strlen(pstr) << endl;
    /* pstr[0] = 'H';//error，不能进行写操作 */
    pstr = "wuhan";
    cout << "pstr = " << pstr << endl;

    cout << endl << endl;
    //需求：如何将字符串str1与str2拼接起来
    /* strcat(str1, str2); */

    size_t len = sizeof(str1) + sizeof(str2); 
    char *ptmp = new char[len]();
    strcpy(ptmp, str1);
    strcat(ptmp, str2);

    cout << "ptmp = " << ptmp << endl;

    delete [] ptmp;
    ptmp = nullptr;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

