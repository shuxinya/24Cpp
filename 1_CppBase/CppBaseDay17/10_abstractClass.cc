#include <iostream>

using std::cout;
using std::endl;

//将构造函数用protected修饰的类是抽象类
class Base
{
/* public: */
private:
protected:
    Base()
    {
        cout << "Base()" << endl;
    }
};

class Derived
: public Base
{
public:
    Derived()
    : Base()
    {
        cout << "Derived()" << endl;
    }
};

void test()
{
    /* Base base;//error */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

