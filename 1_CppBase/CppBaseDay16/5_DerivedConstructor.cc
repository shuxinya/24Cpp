#include <iostream>

using std::cout;
using std::endl;

class Base
{
public:
    Base(long base)
    : _base(base)
    {
        cout << "Base(long)" << endl;
    }
private:
    long _base;

};

class Derived
: public Base
{
public:
};

void test()
{
    Derived d;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

