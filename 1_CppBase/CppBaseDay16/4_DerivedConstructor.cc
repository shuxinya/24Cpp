#include <iostream>

using std::cout;
using std::endl;

class Base
{
public:
    Base()
    {
        cout << "Base()" << endl;
    }

};

class Derived
: public Base
{
public:
    Derived(long derived)
    : Base()
    , _derived(derived)
    {
        cout << "Derived(long)" << endl;
    }
private:
    long _derived;
};

void test()
{
    //"创建派生类对象的时候，会调用基类的构造函数",错误的说法
    //创建派生类对象的时候，会调用派生类的构造函数，但是为了完成
    //从基类吸收过来的数据成员的初始化，所以借助了基类的构造，此时
    //就调用了基类的构造函数，然后在执行派生类构造函数的函数体
    Derived derived(10);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

