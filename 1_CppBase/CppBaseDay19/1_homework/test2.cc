#include "tinyxml2.h"
#include <iostream>
#include <regex>
#include <string>

using std::cout;
using std::endl;
using std::cerr;
using std::regex;
using std::string;

using namespace tinyxml2;

void test()
{
    XMLDocument doc;
    doc.LoadFile("coolshell.xml");

    if(doc.ErrorID())
    {
        cerr << "LoadFile error" << endl;
        return;
    }

    XMLElement *pNode = doc.FirstChildElement("rss")->FirstChildElement("channel")->FirstChildElement("item");
    /* if(pNode) */
    while(pNode)
    {
        string title = pNode->FirstChildElement("title")->GetText();
        string link = pNode->FirstChildElement("link")->GetText();
        string description = pNode->FirstChildElement("description")->GetText();
        string content = pNode->FirstChildElement("content:encoded")->GetText();


        regex reg("<[^>]+>");
        title = regex_replace(title, reg, "");
        link = regex_replace(link, reg, "");
        description = regex_replace(description, reg, "");
        content = regex_replace(content , reg, "");

        cout << "title = " << title << endl;
        cout << "link = " << link  << endl;
        cout << "description = " <<  description << endl;
        cout << "content  = " << content << endl;

        pNode = pNode->NextSiblingElement("item");//获取下一个兄弟节点
    }

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

