#include <iostream>
#include <memory>
#include <vector>

using std::cout;
using std::endl;
using std::unique_ptr;
using std::vector;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    void print() const
    {
        cout << "(" << _ix 
             << ", " << _iy
             << ")" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    unique_ptr<Point> up(new Point(10, 20));//智能指针
    cout << "*up = ";
    up->print();
    cout << "up.get() = " << up.get() << endl;

    cout << endl;
    /* unique_ptr<Point> up2 = up;//复制,error */

    cout << endl;
    unique_ptr<Point> up3(new Point(30, 40));//智能指针

    cout << endl;
    /* up3 = up;//赋值,error */

    vector<unique_ptr<Point>> vec;
    vec.push_back(std::move(up));//作为容器元素的时候，可以传
    vec.push_back(std::move(up3));//右值

    vec.push_back(unique_ptr<Point>(new Point(1, 9)));
}


int main(int argc, char *argv[])
{
    test();
    return 0;
}

