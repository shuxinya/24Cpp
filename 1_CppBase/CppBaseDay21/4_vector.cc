#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}
void test()
{
    cout << "sizeof(vecotr<int>) = " << sizeof(vector<int>) << endl;
    cout << "sizeof(vecotr<long>) = " << sizeof(vector<long>) << endl;

    cout << endl;
    vector<int> number = {1, 2, 4, 6, 8, 9, 3, 6, 7};
    display(number);

    cout << endl << "在vector尾部进行插入与删除" << endl;
    number.push_back(11);
    number.push_back(22);
    display(number);
    number.pop_back();
    display(number);

    //对于vector而言，不能在头部进行插入与删除?为什么？
    //vector是一端开口的,只能在尾部进行操作
    //在头部进行插入与删除的时候，会将后面的元素全部进行移动
    //这样会导致开销比较大，时间复杂度会为O(N)
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

