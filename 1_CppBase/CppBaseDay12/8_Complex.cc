#include <iostream>

using std::cout;
using std::endl;

class Complex
{
    friend Complex operator+(const Complex &lhs, const Complex &rhs);
public:
    Complex(double dreal = 0.0, double dimag = 0.0)
    : _dreal(dreal)
    , _dimag(dimag)
    {
        cout << "Complex(double = 0.0, double = 0.0)" << endl;
    }

    double getReal() const
    {
        return _dreal;
    }

    double getImag() const
    {
        return _dimag;
    }

    void print() const
    {
        cout << _dreal << " + " << _dimag << "i" << endl;
    }

    //运算符重载的第二种形式：成员函数的形式

    ~Complex()
    {
        cout << "~Complex()" << endl;
    }
private:
    double _dreal;
    double _dimag;
};

//运算符重载的第三种形式：友元的形式(对于加号的重载，推荐友元)
//保证参数的个数是两个，可以直接访问私有成员，而不用写get函数
Complex operator+(const Complex &lhs, const Complex &rhs)
{
    cout << "friend Complex operator+(const Complex &, const Complex &)" << endl;
    return Complex(lhs._dreal + rhs._dreal, lhs._dimag + rhs._dimag);
}

void test()
{
    Complex c1(1, 3);
    cout << "c1 = ";
    c1.print();

    cout << endl;
    Complex c2(2, 6);
    cout << "c2 = ";
    c2.print();

    cout << endl;
    Complex c3 = c1 + c2;
    cout << "c3 = ";
    c3.print();
}


int main(int argc, char *argv[])
{
    test();
    return 0;
}

