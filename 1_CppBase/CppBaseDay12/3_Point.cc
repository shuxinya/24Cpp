#include <math.h>
#include <iostream>

using std::cout;
using std::endl;

class Point;//类的前向声明

//友元的性质：友元关系是单向的（A是B的友元，但是B并一定是A的友元）
//友元关系没有传递性(A是B的友元, B是C的友元，但是此时A并不是C的友元 )
//友元关系不能被继承
class Line
{
public:
    double distance(const Point &lhs, const Point &rhs);
    void setPoint(Point &pt, int ix, int iy);
private:
    int _iz;
};

class Point
{
    /* friend double Line::distance(const Point &lhs, const Point &rhs); */
    /* friend void Line::setPoint(Point &pt, int ix, int iy); */
    //友元的第三种形式：友元之友元类形式
    friend class Line;
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    void setLine(Line &line, int iz)
    {
        line._iz = iz;
    }

    void print() const
    {
        cout << "(" << this->_ix
             << ", " << this->_iy
             << ")";
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

double Line::distance(const Point &lhs, const Point &rhs)
{
    return hypot(lhs._ix - rhs._ix, lhs._iy - rhs._iy);
}

void Line::setPoint(Point &pt, int ix, int iy)
{
    pt._ix = ix;
    pt._iy = iy;
}

void test()
{
    Line line;

    Point pt1(1, 2);
    Point pt2(4, 6);

    line.setPoint(pt2, 10, 20);
    //计算两个点之间的距离
    pt1.print();
    cout << "---->";
    pt2.print();
    /* cout << "之间的距离 " << Line().distance(pt1, pt2) << endl; */
    cout << "之间的距离 " << line.distance(pt1, pt2) << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

